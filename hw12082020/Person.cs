﻿using System;
using System.Collections.Generic;

namespace hw12082020
{
    class Person : IComparable<Person>
    {
        private static readonly IComparer<Person> _compareById;
        private static readonly IComparer<Person> _compareByAge;
        private static readonly IComparer<Person> _compareByName;
        private static readonly IComparer<Person> _compareByHeight;
        private static IComparer<Person> _defultComparer; // Etgar


        public int Id { get; private set; }
        public string Name { get; private set; }
        public int Age { get; private set; }
        public float Height { get; private set; }

        static Person()
        {
            _compareById = new PersonCompareById();
            _compareByName = new PersonCompareByName();
            _compareByAge = new PersonSortByAge();
            _compareByHeight = new PersonCompareByHeight();
            DefultComparer = _compareById;

        }

        public Person(int id, string name, int age, float height)
        {
            Id = id;
            Name = name;
            Age = age;
            Height = height;
        }
        public static IComparer<Person> IDComparer
        {
            get
            {
                return _compareById;
            }
        }
        public static IComparer<Person> NameComparer
        {
            get
            {
                return _compareByName;
            }
        }
        public static IComparer<Person> AgeComparer
        {
            get
            {
                return _compareByAge;
            }
        }
        public static IComparer<Person> HeightComparer
        {
            get
            {
                return _compareByHeight;
            }
        }

        public static IComparer<Person> DefultComparer // Etgar
        {
            get
            {
                return _defultComparer;
            }
            set
            {
                _defultComparer = value;
            }
        }


        public int CompareTo(Person b)
        {
           return DefultComparer.Compare(this, b); //Etgar
        }

        public override string ToString()
        {
            return $"ID: {Id} Name: {Name} Age: {Age} Height: {Height}";
        }
    }


}


