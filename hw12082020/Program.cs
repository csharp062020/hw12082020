﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw12082020
{
    class Program
    {
        static void Main(string[] args)
        {
            HederPrint("start");
            Person[] persons =
            {
                new Person(1, "a", 1, 1.1f),
                new Person(5, "b", 8, 1.8f),
                new Person(3, "c", 5, 5.0f),
                new Person(5, "d", 20, 3f),
                new Person(2, "e", 3, 2.2f)
            };

            HederPrintAndSortAndPrint("original array",persons);
            HederPrintAndSortAndPrint("Sort array By Defult  - Etgar", persons,true);
            HederPrintAndSortAndPrint("Sort By Person.NameComparer ", persons, true,Person.NameComparer);
            HederPrintAndSortAndPrint("Sort array By Person.AgeComparer  - Etgar", persons, true,Person.AgeComparer);
            HederPrintAndSortAndPrint("Sort array By Person.IDComparer  - Etgar", persons, true, Person.IDComparer);
            HederPrintAndSortAndPrint("Sort array By Person.AgeComparer  - Etgar", persons, true, Person.HeightComparer);
        }

        static void PrintPersonArray(Person[] persons)
        {
            foreach (Person p in persons)
            {
                Console.WriteLine(p);
            }

        }

        private static void HederPrint(string heder)
        {
            Console.WriteLine("\n------------------------------");
            Console.WriteLine($"{heder}");
            Console.WriteLine("------------------------------\n");
        }

        static void HederPrintAndSortAndPrint(string heder, Person[] persons ,bool sortOn = false, IComparer<Person> costumComparer =null)
        {
            HederPrint(heder);
            if (sortOn)
            {
                if (costumComparer != null)
                {
                    Array.Sort(persons, costumComparer);
                }
                else
                {
                    Array.Sort(persons);
                }
            }

            PrintPersonArray(persons);
        }


    }
}
