﻿using System.Collections.Generic;

namespace hw12082020
{
    class PersonCompareById : IComparer<Person>
    {
        public int Compare(Person a, Person b)
        {
            return a.Id - b.Id;
        }
    }

    class PersonSortByAge : IComparer<Person>
    {
        public int Compare(Person a, Person b)
        {
            return a.Age - b.Age;
        }
    }

    class PersonCompareByName : IComparer<Person>
    {
        public int Compare(Person a, Person b)
        {
            return a.Name.CompareTo(b.Name);
        }
    }

    class PersonCompareByHeight : IComparer<Person>
    {
        public int Compare(Person a, Person b)
        {
            if (a.Height == b.Height)
            {
                return 0;

            }
            else if (a.Height > b.Height)
            {
                return 1;
            }
            else // a.Height < b.Height
            {
                return -1;
            }
        }

    }

}